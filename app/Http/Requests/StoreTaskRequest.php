<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreTaskRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        return [
            'name' => [
                'required',
                Rule::unique('tasks')->where(function ($query) use ($user) {
                    return $query->where('user_id', $user->id);
                })
            ],
            'position' => [
                'required',
                'integer',
            ],
        ];
    }
}
