@extends('layout')

@section('title', 'Create Category')

@section('body')

@include('partials/top-nav', [
    'title' => 'Categories'
])

@include('partials/errors')

    <form action="{{ route('categories.store') }}" method="post">
        @csrf()
        <label for="name">Name</label>
        <input type="text" name="name" id="name" autofocus>

        <label for="position">Position</label>
        <input type="number" name="position" id="position">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Create">
    </form>
@endsection
