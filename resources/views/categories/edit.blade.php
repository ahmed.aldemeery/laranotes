@extends('layout')

@section('title', 'Edit Category')

@section('links')
<a href="{{ route('categories.show', ['category' => $category['id']]) }}">View</a> |
<form class="delete-form" method="post" action="{{ route('categories.destroy', ['category' => $category['id']]) }}">
    @method('DELETE')
    @csrf()

    <input class="delete-btn" type="submit" value="Delete">
</form>
@endsection

@section('body')

@include('partials/top-nav', [
    'title' => 'Categories'
])

@include('partials/errors')
    <form action="{{ route('categories.update', [
        'category' => $category['id']
    ]) }}" method="post">
        @method('PUT')
        @csrf()
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $category['name'] }}">

        <label for="position">Position</label>
        <input type="number" name="position" id="position" value="{{ $category['position'] }}">

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Update">
    </form>
    @endsection
