@extends('layout')

@section('title', 'All Cateogries')

@section('links')
<a href="{{ route('categories.create') }}">New Category</a>
@endsection

@section('body')
    @include('partials/top-nav', [
        'title' => 'Categories'
    ])


    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Position</th>
            <th>Actions</th>
        </tr>


        @foreach ($categories as $category)
        <tr>
            <td>{{ $category['id'] }}</td>
            <td>{{ str_limit($category['name'], 10) }}</td>
            <td>{{ $category['position'] }}</td>
            <td>
                <a href="{{ route('categories.edit', [
                    'category' => $category['id']
                ]) }}">Edit</a> |
                <a href="{{ route('categories.show', [
                    'category' => $category['id']
                ]) }}">View</a> |

                <form class="delete-form" method="post" action="{{ route('categories.destroy', [
                    'category' => $category['id']
                ]) }}">
                    @method('DELETE')
                    @csrf()

                    <input class="delete-btn" type="submit" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach

    </table>


    @endsection


