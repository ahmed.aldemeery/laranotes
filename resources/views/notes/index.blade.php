@extends('layout')

@section('title', 'All Notes')

@section('links')
<a href="{{ route('notes.create') }}">New Note</a>
@endsection

@section('body')
    @include('partials/top-nav', [
        'title' => 'Notes'
    ])

    <table>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Content</th>
            <th>Position</th>
            <th>Actions</th>
        </tr>
        @foreach ($notes as $note)
        <tr>
            <td>{{ $note['id'] }}</td>
            <td>{{ str_limit($note['title'], 10) }}</td>
            <td>{{ str_limit($note['content'], 10) }}</td>
            <td>{{ $note['position'] }}</td>
            <td>
                <a href="{{ route('notes.edit', [
                    'note' => $note['id']
                ]) }}">Edit</a> |
                <a href="{{ route('notes.show', [
                    'note' => $note['id']
                ]) }}">View</a> |
                <form class="delete-form" method="post" action="{{ route('notes.destroy', [
                    'note' => $note['id']
                ]) }}">
                    @method('DELETE')
                    @csrf()

                    <input class="delete-btn" type="submit" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach
    </table>
@endsection
