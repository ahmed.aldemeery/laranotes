@extends('layout')

@section('title', 'Create Task')

@section('body')

@include('partials/top-nav', [
    'title' => 'Tasks'
])

@include('partials/errors')

    <form action="{{ route('tasks.store') }}" method="post">
        @csrf()
        <label for="name">Name</label>
        <input type="text" name="name" id="name" autofocus>

        <label for="position">Position</label>
        <input type="number" name="position" id="position">

        <label for="done">Done</label>
        <input type="checkbox" name="done" id="done">

        <br />
        <br />

        <input type="reset" value="Rest" style="float: left;">
        <input type="submit" value="Create">
    </form>
@endsection
