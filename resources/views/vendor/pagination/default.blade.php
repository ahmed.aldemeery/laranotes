@if ($paginator->hasPages())
    <nav>
        <ul class="pagination" style="list-style-type: none;">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li style="float: left; padding: 0 3px;" class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true">&lsaquo;&lsaquo; Previous</span>
                </li>
            @else
                <li style="float: left; padding: 0 3px;">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;&lsaquo; Previous</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li style="float: left; padding: 0 3px;" class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li style="float: left; padding: 0 3px;" class="active" aria-current="page"><span>{{ $page }}</span></li>
                        @else
                            <li style="float: left; padding: 0 3px;"><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li style="float: left; padding: 0 3px;">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">Next &rsaquo;&rsaquo;</a>
                </li>
            @else
                <li style="float: left; padding: 0 3px;" class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span aria-hidden="true"> Next &rsaquo;&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
